//
//  TestController.swift
//  
//
//  Created by Yudiz Solutions Pvt. Ltd. on 07/10/22.
//

import Foundation
import UIKit
import SDWebImage

class TestController: UIViewController {
    
    ///Oultale(S)
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    ///Variables
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
    }
}

//MARK:- UI Related Methode(s)
extension TestController {
    func prepareUI() {
        lblTitle.text = "Screen From Framework"
        imgView.sd_setImage(with: URL(string: "https://source.unsplash.com/user/c_v_r/1900x800")!, placeholderImage: nil, options: .refreshCached, completed: nil)
    }
}

//MARK:- Action Related Methode(s)
extension TestController {
    
}

//MARK:- UITableView Related Methode(s)
extension TestController {
    
}

//MARK:- API Related Methode(s)
extension TestController {
    
}

