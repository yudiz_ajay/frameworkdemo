//
//  FrameworkManager.swift
//  DemoFramework
//
//  Created by Yudiz Solutions Pvt. Ltd. on 07/10/22.
//

import UIKit

public class FrameworkManager {
    
    public static let shared = FrameworkManager()
    
    public func loadInitialController() -> UIViewController? {
        if let vc = UIStoryboard(name: "Entry", bundle: Bundle(for: TestController.self)).instantiateInitialViewController(){
            return vc
        }
        return nil
    }
}
